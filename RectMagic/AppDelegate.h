//
//  AppDelegate.h
//  RectMagic
//
//  Created by kurupareshan pathmanathan on 1/16/20.
//  Copyright © 2020 kurupareshan pathmanathan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

