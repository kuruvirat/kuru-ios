//
//  main.m
//  RectMagic
//
//  Created by kurupareshan pathmanathan on 1/16/20.
//  Copyright © 2020 kurupareshan pathmanathan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
