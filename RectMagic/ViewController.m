//
//  ViewController.m
//  RectMagic
//
//  Created by kurupareshan pathmanathan on 1/16/20.
//  Copyright © 2020 kurupareshan pathmanathan. All rights reserved.
//

#import "ViewController.h"
#import "Masonry.h"

@interface ViewController ()

@end

@implementation ViewController {
    
    UIView *_box1;
    UIImageView *_box2;
    NSLayoutConstraint *_centerXConstraintBox2,*_centerYConstraintBox2;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
   
    _box1 = [[UIView alloc] init];
    [self.view addSubview: _box1];
    _box1.backgroundColor = [UIColor blackColor];
    
    [_box1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.width.equalTo(@300);
        make.height.equalTo(@300);
        
    }];
    
    _box2 = [[UIImageView alloc] init];
    _box2.image =[UIImage imageNamed:@"dhoni"];
    [self.view addSubview: _box2];
    _box2.backgroundColor = [UIColor greenColor];
    _box2.translatesAutoresizingMaskIntoConstraints = NO;
    _box2.userInteractionEnabled = YES;
    
    _centerXConstraintBox2 = [NSLayoutConstraint constraintWithItem:_box2 attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    
    [self.view addConstraint:_centerXConstraintBox2];
    
    _centerYConstraintBox2 = [NSLayoutConstraint constraintWithItem:_box2 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    
    [self.view addConstraint:_centerYConstraintBox2];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_box2 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:200]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_box2 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:200]];
    
  
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleTheImageUsingScale:)];
    [_box2 addGestureRecognizer:pinch];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movingImageLocationUsingPan:)];
    [_box2 addGestureRecognizer:pan];
    
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotateImage:)];
    [_box2 addGestureRecognizer:rotate];
    
}

- (void)movingImageLocationUsingPan:(UIPanGestureRecognizer *)pan {
    
    CGPoint translation = [pan translationInView:pan.view];
    
    _centerXConstraintBox2.constant += translation.x;
    _centerYConstraintBox2.constant += translation.y;
    
    [pan setTranslation:CGPointZero inView:pan.view];
    
    [self minimizingTranslationValueInsideTheRect];
    
}

- (void)rotateImage:(UIRotationGestureRecognizer *)rotate {
    
    CGAffineTransform rotateTransform = CGAffineTransformRotate(rotate.view.transform, rotate.rotation);
    _box2.transform = rotateTransform;
    rotate.rotation = 0.0;
    
    [self minimizingTranslationValueInsideTheRect];
    [self minimizingScaleValue];
    
}

- (void)scaleTheImageUsingScale:(UIPinchGestureRecognizer *) pinch {
    
    CGAffineTransform scaleTransform = CGAffineTransformScale(_box2.transform, pinch.scale, pinch.scale);
    _box2.transform = scaleTransform;
    pinch.scale = 1.0;
    
    [self minimizingTranslationValueInsideTheRect];
    [self minimizingScaleValue];
    
}

- (void)minimizingTranslationValueInsideTheRect {
    
    if (!(CGRectContainsRect(_box1.frame, _box2.frame))) {
        
        CGPoint pointOverTheRect = CGPointMake(_centerXConstraintBox2.constant,_centerYConstraintBox2.constant);
        
        if (CGRectGetMinX(_box2.frame) <= CGRectGetMinX(_box1.frame)) {
            pointOverTheRect.x = fabs(_box2.frame.origin.x - _box1.frame.origin.x) + pointOverTheRect.x;
        }
        else if (CGRectGetMaxX(_box2.frame) >= CGRectGetMaxX(_box1.frame)) {
             pointOverTheRect.x = fabs(_box2.frame.origin.x - _box1.frame.origin.x) - pointOverTheRect.x;
        }
        if (CGRectGetMinY(_box2.frame) <= CGRectGetMinY(_box1.frame)) {
            pointOverTheRect.y = fabs(_box2.frame.origin.y - _box1.frame.origin.y) + pointOverTheRect.y;
        }
        else if (CGRectGetMaxY(_box2.frame) >= CGRectGetMaxY(_box1.frame)) {
             pointOverTheRect.y = fabs(_box2.frame.origin.y - _box1.frame.origin.y) - pointOverTheRect.y;
        }
        
        _centerXConstraintBox2.constant = pointOverTheRect.x;
        _centerYConstraintBox2.constant = pointOverTheRect.y;
        
    }
    
}

- (void)minimizingScaleValue {
    
    if (!(CGRectContainsRect(_box1.frame, _box2.frame))) {
     
        CGFloat hightDiff = _box1.frame.size.height - _box2.frame.size.height;
        CGFloat widthDiff = _box1.frame.size.width - _box2.frame.size.width;
        
        CGFloat heightScale = (_box2.frame.size.height + hightDiff) / _box2.frame.size.height;
        CGFloat widthScale = (_box2.frame.size.width + widthDiff) / _box2.frame.size.width;
        
        CGFloat minimumScale = MIN(heightScale, widthScale);
        
        CGAffineTransform validatedScaleTransform = CGAffineTransformScale(_box2.transform, minimumScale, minimumScale);
        
        _box2.transform = validatedScaleTransform;
        
    }
    
    
}

@end
